import express from 'express';
import fs from 'fs';

const app = express();

app.get('/', (req, res) => {
    let inputFile = './db/data/data.json',
        readStream = fs.createReadStream(inputFile);

    readStream.pipe(res);
});

const server = app.listen(3000, () => {
    console.log('Server running on http://localhost:', server.address().port);
});
