var gulp   = require('gulp');
var jshint = require('gulp-jshint');
var watch = require('gulp-watch');

var jsFiles = ['index.js', 'gulpfile.js'];

gulp.task('default', () => {
    return watch(jsFiles, () => {
        gulp.src(jsFiles)
            .pipe(jshint())
            .pipe(jshint.reporter('default', { verbose: true }));
        });
});

gulp.task('lint', function() {
  return gulp.src(jsFiles)
    .pipe(jshint())
    .pipe(jshint.reporter('default', { verbose: true }));
});